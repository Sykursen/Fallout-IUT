unit UnitCentre;

{$mode objfpc}{$H+}  {$codepage UTF8}

interface

uses
  Classes, SysUtils,unitRessources,GestionEcran,Unitcoordonnee,UnitCadre,unitReponse;

function texteCentre():string;                                                  //Change le texte en fonction de la construction du centre
procedure InitialisationCentre;                                                 //Initialise le centre à faux au debut de partie
procedure Constructioncentre;                                                   //Gère la  construction du centre
Procedure cliqueCentre;                                                         //Gère les sortie possibles quand l'utilisateur entre dans la partie centre
function getEtatlevel2():boolean;                                               //Renvoie les etats
function getEtatlevel3():boolean;                                               //Renvoie les etats
procedure recherche();                                                          //page du centre de recherche
procedure level2 ;                                                              //Gère la premiere amélioration disponible
procedure level3 ;                                                              //Gère la 2eme amélioration disponible
function etatConstruction2centre():string;                                      //Texte dans la partie synthese

implementation
uses
  UnitMenu;
var
  EtatconstructionCentre:boolean;                                               //Etat de construction du centre technique
  Etatlevel2:boolean;                                                           //Etat avancement recherche  level 2
  Etatlevel3:boolean;                                                           //Etat avancement recherche level 3

function texteCentre():string;
 begin
      if EtatconstructionCentre=true then
      texteCentre:='Utiliser le centre de Recherche'
      else  texteCentre:='Construire le centre de Recherche (100 aciers)';
 end;

procedure InitialisationCentre;                                                 //Initialise le centre à faux au debut de partie
 begin
      EtatconstructionCentre:=false;
      Etatlevel2:=false;
      Etatlevel3:=false;
 end;

procedure Constructioncentre;
begin
     if getsteel>=100 then
     begin
          EtatconstructionCentre:=True;
          setsteel(getsteel-100);
          menuPrincipal;
     end
     else
     begin
          ecrireEnPosition(geterreurcoord,'Vous n''avez pas assez d''aciers');
          readln();
          menuprincipal();
     end;
end;

Procedure cliqueCentre;
begin
     if EtatconstructionCentre=True then recherche
     else Constructioncentre;
end;

function getEtatlevel2():boolean;
begin
     getEtatlevel2:=Etatlevel2
end;

function getEtatlevel3():boolean;
begin
     getEtatlevel3:=Etatlevel3
end;

procedure recherche();
var
  reprecherche : string;
begin

  effacerEcran;
  reprecherche:='0';
  cadre(double);
  deplacerCurseurXY(1,1);
  writeln('Amélioration disponible');


  deplacerCurseurXY(1,2);

  writeln('1: Débloquer l''expérience jusqu''au level 20 (10 composants)');
  deplacerCurseurXY(1,3);
  writeln('2: Débloquer l''expérience jusqu''au level 30 (20 composants)');
  deplacerCurseurXY(1,4);
  writeln('3: Retour');


  synthese();

  // boucle de la réponse
  while (repRecherche<>('1')) AND (repRecherche<>('2')) AND (repRecherche<>('7')) do
        begin
             posRep(repRecherche);
             case repRecherche of
                  '1' : level2;                                                 //Achat bois
                  '2' : level3;                                                 //Achat nourriture
                  '3' : menuprincipal();                                        //Achat acier
             else ecrireEnPosition(getErreurCoord,'Erreur');                    //Pour tout autre nombre entré alors afficher "erreur"
             end;
        end
end;

procedure level2;
begin
  if Etatlevel2=true then
  begin
       ecrireEnPosition(getErreurCoord,'Déja débloqué');
       readln();
  end
  else
      begin
           if getcomposant>=10 then
           begin
                Etatlevel2:=true;
                setcomposant(getcomposant-10)
           end
           else
           begin
                ecrireEnPosition(geterreurcoord,'Vous n''avez pas assez de composants');
                readln();
           end;
       end;
      recherche();
end;

procedure level3;
begin
  if Etatlevel3=true then
     begin
          ecrireEnPosition(getErreurCoord,'Déja débloqué');
          readln();
     end
  else
      begin
           if getcomposant>=20 then
           begin
                Etatlevel3:=true;
                setcomposant(getcomposant-20)
           end
           else
           begin
                ecrireEnPosition(geterreurcoord,'Vous n''avez pas assez de composants');
                readln();
           end;
       end;
    recherche();
end;

function etatConstruction2centre ():string;
begin
     if EtatconstructionCentre=false then
       etatConstruction2centre:='Non'
     else etatConstruction2centre:='Oui';
end;
end.

