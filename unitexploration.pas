unit unitExploration;

{$mode objfpc}{$H+}  {$codepage UTF8}

interface

uses
  Classes,SysUtils,unitRessources,GestionEcran,unitMeteo,unitCadre,unitGestionPersonnage;

procedure initialisationExplo();                                                //procedure définissant le nombre de personnes restant au camp
procedure ExplorationLac();                                                     //procedure définissant le nombre de personnes au lac
procedure ExplorationForet();                                                   //procedure définissant le nombre de personnes dans la forêt
procedure ExplorationIUT();                                                     //procedure définissant le nombre de personnes à l'iut
procedure ExplorationDecharge();                                                //procedure définissant le nombre de personnes à la décharge
procedure bilanRessource();                                                     //Ajoute les ressources à chaque fin de tour
procedure exploitation();                                                       //procedure permettant d'exploiter la ferme pour récolter de la nourriture
function getPersonnesInactives():integer ;                                      //fonction renvoyant le nombre de personnes inactives
function getPersonnesAuLac():integer;                                           //fonction renvoyant le nombre de personnes en exploration au lac
function getPersonnesEnForet():integer ;                                        //fonction renvoyant le nombre de personnes en exploration dans la forêt
function getPersonnesAIUT():integer;                                            //fonction renvoyant le nombre de personnes en exploration à l'IUT
function getPersonnesEnExploitationFerme():integer;                             //fonction renvoyant le nombre de personnes en train d'exploiter la ferme
function getPersonnesALaDecharge():integer;                                     //fonction renvoyant le nombre de personnes en train d'exploiter la décharge
procedure setLieuchoisipourattribution(Valeur:OccupationEnu);                   //Fonction qui permet de changer quelle zone le joueur veut explorer
function getLieuchoisipourattribution():OccupationEnu;                          //Fonction qui renvoie la zone choisie par le joueur
procedure setPersonnesInactives(valeur:Integer);                                //Setter des personnes inactives
procedure ecranBilan();                                                         //procedure permettant d'afficher les ressources collectées à chaque fin de tour
implementation
uses
  UnitMenu;

var
   PersonnesInactives:integer;                                                  //personnes qui ne font rien
   PersonnesAuLac:integer;                                                      //personnes en exploration au lac
   PersonnesEnForet:integer;                                                    //personnes en exploration dans la forêt
   PersonnesAIUT:integer;                                                       //personnes en exploration à l'iut
   personnesALaDecharge:integer;                                                //personnes à la décharge
   foodRecupere:integer;                                                        //nourriture récupérée en exploration
   woodRecupere:integer;                                                        //bois récupéré en exploration
   steelRecupere:integer;                                                       //acier récupéré à la décharge
   composantRecupere:Integer;
   PersonnesEnExploitation:Integer;                                             //personnes qui exploitent la ferme
   Lieuchoisipourattribution:OccupationEnu;                                     //lieux choisi par l'utilisateur

procedure initialisationExplo ();
begin
     PersonnesInactives:=4;                                                      //tout le monde est inactif au camp au début de chaque tour
     PersonnesAuLac:=0 ;
     PersonnesEnForet:=0;
     PersonnesAIUT:=0;
     PersonnesALaDecharge:=0;
     PersonnesEnExploitation:=0;
     foodRecupere:=0;
     woodRecupere:=0;
     steelRecupere:=0;
end;

procedure ExplorationLac ();
begin
     if PersonnesInactives>0 then
        begin

             Lieuchoisipourattribution:=Lac;
             //Cadre choix des personnage
             attributionPersonnage;
             PersonnesAuLac:=PersonnesAuLac+1;
             PersonnesInactives:= PersonnesInactives-1;
             randomize;
             //Au lac on récupère entre 1 et 10 de nourriture par personne
             foodRecupere:=PersonnesAuLac*(random(5)+1);
             menuPrincipal;
        end

end;

procedure ExplorationForet ();
begin
     if PersonnesInactives>0 then
        begin
             Lieuchoisipourattribution:=Foret ;
             //Cadre choix des personnage
             attributionPersonnage ;
             PersonnesEnForet:=PersonnesEnForet+1;
             PersonnesInactives:= PersonnesInactives-1;
             randomize;
             //Dans la forêt on récupère entre 1 et 10 de bois par personne
             woodRecupere:=PersonnesEnForet*(random(10)+1);
             menuPrincipal;
        end
end;

procedure ExplorationIUT ();
begin
     if PersonnesInactives>0 then
        begin

             Lieuchoisipourattribution:=IUT ;
             //Cadre choix des personnage
             attributionPersonnage;
             PersonnesAIUT:=PersonnesAIUT+1;
             PersonnesInactives:= PersonnesInactives-1;
             randomize;
             //A l'IUT on récupère entre 1 et 5 de bois et entre 1 et 5 de nourritures et entre 0 et 2 composants
             woodRecupere:=PersonnesAIUT*(random(5)+1);
             foodRecupere:=PersonnesAIUT*(random(5)+1);
             composantrecupere:=PersonnesAIUT*(random(2));
             menuPrincipal;
        end
end;

procedure ExplorationDecharge();
  begin
   if PersonnesInactives>0 then
        begin
             Lieuchoisipourattribution:=Decharge;
             //Cadre choix des personnage
             attributionPersonnage;
             PersonnesALaDecharge:=PersonnesALaDecharge+1;
             PersonnesInactives:= PersonnesInactives-1;
             randomize;
             //A la decharge  on récupère entre 1 et 10 d'aciers
             steelRecupere:=PersonnesALaDecharge*(random(10)+1);
             menuPrincipal;
        end


 end;

procedure bilanRessource();
begin
      setWood(getWood+woodRecupere);
      setFood(getFood+foodRecupere);
      setSteel(getSteel+steelRecupere);
      setcomposant(getcomposant+composantrecupere)
end;

procedure exploitation();
begin
     if PersonnesInactives>0 then
        begin

             Lieuchoisipourattribution:=Ferme;
             //cadre choix personnage
             attributionPersonnage;
             PersonnesEnExploitation:=PersonnesEnExploitation+1;
             PersonnesInactives:= PersonnesInactives-1;
             //Impact de la méteo sur les ressource
             case getMeteo() of
                  Nuageux : foodRecupere:=PersonnesEnExploitation*(random(7)+1);
                  Averses : foodRecupere:=PersonnesEnExploitation*(random(5)+1);
                  Soleil : foodRecupere:=PersonnesEnExploitation*(random(8)+1);
                  Pluies_Acides : foodRecupere:=PersonnesEnExploitation*1;
             end;
             menuPrincipal;
        end;
end;

function getPersonnesInactives():integer;
begin
     getPersonnesInactives:=PersonnesInactives;
end;

function getPersonnesAuLac():integer;
begin
    getPersonnesAuLac:=PersonnesAuLac;
end;

function getPersonnesEnForet():integer;
begin
     getPersonnesEnForet:=PersonnesEnForet;
end;

function getPersonnesAIUT():integer ;
begin
    getPersonnesAIUT:=PersonnesAIUT;
end;

function getPersonnesALaDecharge():integer;
begin
    getPersonnesALaDecharge:=personnesALaDecharge;
end;

function getPersonnesEnExploitationFerme():integer;
begin
      getPersonnesEnExploitationFerme:=PersonnesEnExploitation;
end;

function getLieuchoisipourattribution():OccupationEnu;
begin
    getLieuchoisipourattribution:=Lieuchoisipourattribution
end;

procedure setLieuchoisipourattribution(Valeur:OccupationEnu);
begin
     Lieuchoisipourattribution:=Valeur;
end;

procedure setPersonnesInactives(valeur:Integer);
begin
     PersonnesInactives:=valeur;
end;

procedure ecranBilan();
 begin
     effacerEcran;
     //décor
     cadre(double);
     couleurTexte(10);

     //texte
     deplacerCurseurXY(1,3);
      write('Bois récupéré: ',woodRecupere);
      deplacerCurseurXY(1,4);
      write('Nourriture récupérée: ',foodRecupere);
      deplacerCurseurXY(1,5);
      write('Acier récupéré: ',steelRecupere);
      deplacerCurseurXY(1,6);
      write('Composants récupérées: ',composantRecupere);


      BonusressourceEXP();
      deplacerCurseurXY(0,0);
      readln;
 end;

end.

