unit unitRessources;

{$mode objfpc}{$H+} {$codepage UTF8}

interface

uses
  Classes, SysUtils;


procedure initialisation();            //Inialisation des ressources en début de partie
function getFood() : Integer;          //Renvoie la valeur de la variable food
function getWood() : Integer;          //Renvoie la valeur de la variable wood
function getSteel() : Integer;         //Renvoie la valeur de la variable steel
function getMoney() : Integer;         //renvoie la valeur de la variable money
function getComposant() : Integer;         //renvoie la valeur de la variable composant

procedure setFood(valeur : integer);   //Modifie la valeur de la variable food
procedure setWood(valeur : integer);   //Modifie la valeur de la variable wood
procedure setSteel(valeur : integer);  //Modifie la valeur de la variable steel
procedure setMoney(valeur : integer);  //Modifie la valeur de la variable monney
procedure setComposant(valeur : Integer);     //modifie la valeur de la variable composant

function qtwoodSTR():string;           //Fonction qui écrit une string avec le nombre de bois (pour la fonction écrire en position)
function qtfoodSTR():string;           //Fonction qui écrit une string avec le nombre de nourriture (pour la fonction écrire en position)
function qtSteelSTR():string;          //Fonction qui écrit une string avec le nombre d'acier (pour la fonction écrire en position)
function qtMoneySTR():string;          //Fonction qui écrit une string avec le nombre d'argent (pour la fonction écrire en position)
function qtComposantSTR():string;          //Fonction qui écrit une string avec le nombre de composant (pour la fonction écrire en position)

procedure manger();                    //Fonction qui retire 4 de nourriture

implementation

var
    wood:Integer;                    //Bois
    food:Integer;                    //Nourriture
    steel:integer;                   //Acier
    money:integer;                   //Argent
    composant:Integer;               //composant de recherche

procedure initialisation();
begin
  food := 10;
  wood := 10;
  steel := 10;
  money := 0;
end;

function getFood() : Integer;
begin
     getFood := food;
end;

function getwood() : Integer;
begin
     getwood := wood;
end;

function getSteel() : Integer;
begin
     getSteel:=steel;
end;

function getMoney() : Integer;
begin
     getMoney:=money;
end;

function getComposant() : Integer;
begin
     getcomposant:=composant;
end;

procedure setFood(valeur : integer);
begin
     food := valeur;
end;

procedure setWood(valeur : integer);
begin
     wood := valeur;
end;

procedure setSteel(valeur : integer);
begin
    steel := valeur;
end;

procedure setMoney(valeur : integer);
begin
      money := valeur;
end;

procedure setComposant(valeur: integer);
begin
     composant:=valeur;
end;

function qtfoodSTR():string;
begin
     qtfoodSTR:=('Quantité de nourriture : ')+(inttostr(getFood));
end;

function qtwoodSTR():string;
begin
     qtwoodSTR:=('Quantité de bois : ')+(inttostr(getWood));
end;

function qtSteelSTR():string;
begin
     qtSteelSTR:=('Quantité d''acier : ')+(inttostr(getSteel));
end;

function qtMoneySTR():string;
begin
      qtMoneySTR:=('Quantité d''argent : ')+(inttostr(getMoney));
end;

function qtcomposantSTR():string;
begin
      qtComposantSTR:=('Quantité de composant : ')+(inttostr(getcomposant));
end;

procedure manger();
begin
     if getFood>4 then setFood(getFood-4)
     else setFood(0);
end;

end.

