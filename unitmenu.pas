unit unitMenu;

{$mode objfpc}{$H+}  {$codepage UTF8}

interface
uses
  Classes, SysUtils,GestionEcran,unitRessources,unitCoordonnee,unitCadre,
  unitGestionPersonnage,unitReponse,unitDate,unitexploration,unitCombat,unitFerme,unitMeteo,unitMarchand,unitDecouverte,unitCheat,UnitCentre;

procedure intro();                                                              //Procédure qui génère l'intro
procedure credits();                                                            //Procédure qui génère les crédits
procedure preface();                                                            //Procédure qui gère l'affichage de la préface
procedure menuIni();                                                            //Procedure gérant le menu initial
procedure menuPrincipal();                                                      //Procedure gérant le menu principal
procedure debutPartie();                                                        //Procedure qui initialise les données au lancement d'une partie
procedure finDeTour();                                                          //Procedure de fin de tour
procedure synthese();                                                           //procédure qui affiche la synthèse des ressources,etc... à droite de l'écran

implementation

procedure intro();
begin
  cadre(double);
  couleurTexte(10);
  dessinerCadreXY(29,13,89,21,double,2,0);
  titre('_ ._  _ , _ ._',0);
  titre('(_ '' ( `  )_  .__)',1);
  titre('( (  (    )   `)  ) _)',2);
  titre('(__ (_   (_ . _) _) ,__)',3);
  titre('`~~`\ '' . /`~~`',4);
  titre(';   ;',5);
  titre('/   \',6);
  titre('_____________/_ __ \_____________',7);
  titre(' ______    _ _             _     _____ _    _ _______ ',10);
  titre('|  ____|  | | |           | |   |_   _| |  | |__   __|',11);
  titre('| |__ __ _| | | ___  _   _| |_    | | | |  | |  | |   ',12);
  titre('|  __/ _` | | |/ _ \| | | | __|   | | | |  | |  | |   ',13);
  titre('| | | (_| | | | (_) | |_| | |_   _| |_| |__| |  | |   ',14);
  titre('|_|  \__,_|_|_|\___/ \__,_|\__| |_____|\____/   |_|   ',15);
  deplacerCurseurXY(0,0);
  readln;
  effacerEcran();
end;

procedure credits();
begin
  cadre(double);
  titre('_ ._  _ , _ ._',0);
  titre('(_ '' ( `  )_  .__)',1);
  titre('( (  (    )   `)  ) _)',2);
  titre('(__ (_   (_ . _) _) ,__)',3);
  titre('`~~`\ '' . /`~~`',4);
  titre(';   ;',5);
  titre('/   \',6);
  titre('_____________/_ __ \_____________',7);
  titre('Réalisé par :',10);
  titre('Julie Kazmierczak',12);
  titre('Alois Mandry',13);
  titre('Fabien Duc',14);
  titre('Guillaume Assier',15);
  deplacerCurseurXY(0,0);
  readln;
  effacerEcran();
end;

procedure preface();
begin
  cadre(double);
  couleurTexte(10);
  //P1
  titre('Préface',0);
  deplacerCurseurXY(2,8);
  writeln('2063, les abris sont prêts, la probabilité d''une guerre nucléaire grandit. Le gouvernement américain lance donc une');
  deplacerCurseurXY(2,9);
  writeln('campagne d''exercices d''évacuations réguliers, ce qui permet de se rendre compte que le nombre d''Abris est largement');
  deplacerCurseurXY(2,10);
  writeln('insuffisant pour l''ensemble de la population. Une sélection est nécessaire, choisir les meilleurs candidats parmi l''');
  deplacerCurseurXY(2,11);
  writeln('élite de la nation. Vous faites partie des élus.');
  //P2
  deplacerCurseurXY(2,13);
  writeln('2066, la guerre entre la Chine et les USA éclate, et c’est le début de la fin. Malgré les défenses importantes les');
  deplacerCurseurXY(2,14);
  writeln('Chinois l’emportent.');
  //P3
  deplacerCurseurXY(2,16);
  writeln('2076, la guerre nucléaire éclate, seuls les élus survivent !');
  deplacerCurseurXY(2,17);
  couleurTexte(12);
  writeln('La terre est ravagé, les continents sont détruits, les eaux sont polluées.');
  couleurTexte(10);
  //P4
  deplacerCurseurXY(2,19);
  writeln('2112, les vivres se font rares, les systèmes de filtration de l’air, de l’eau dysfonctionnent.');
  deplacerCurseurXY(2,20);
  writeln('Vous êtes choisi pour explorer ce qu’il reste du monde extérieur, trouver des solutions pour faire vivre le reste de');
  deplacerCurseurXY(2,21);
  writeln('l’humanité.');
  deplacerCurseurXY(2,22);
  writeln('Tout au long de votre aventure, vous découvrirez que vous n’êtes pas seul, que l’environnement malgré les change');
  deplacerCurseurXY(2,23);
  writeln('-ments, reste viable, mais tout est à reconstruire.');
  titre('Votre aventure commence là ou celle des autres se termine.',21);
  deplacerCurseurXY(0,0);
  readln;
  effacerEcran();
end;

procedure menuIni();
var
//Variable pour la réponse des menus
RepIntro:string;

begin

     RepIntro:='0';
     intro();
     //décor
     cadre(double);
     couleurTexte(10);

     //Texte
     deplacerCurseurXY(1,1);
     write('1: Commencer une partie' )  ;
     deplacerCurseurXY(1,2);
     write('2: Quitter et afficher les crédits' );


  titre('_.-^^---....,,--',3);
  titre('_--                  --_',4);
  titre('<                        >)',5);
  titre('|                         |',6);
  titre('\._                   _./',7);
  titre('```--. . , ; .--''''''',8);
  titre('| |   |',9);
  titre('.-=||  | |=-.',10);
  titre('`-=#$%&%$#=-''',11);
  titre('| ;  :|',12);
  titre('_____.,-#%&$@%#&#~,._____',13);
  titre('Bienvenue dans le nouveau monde',13);


     //Boucle réponse texte
     while (RepIntro<>('1'))and (RepIntro<>('2')) do
           begin
               posRep(RepIntro);
                    case RepIntro of
                    '1' : debutpartie();                                        //Si rep=1 alors on lance l'initialisation
                    '2' : begin                                                 //Si rep=2 alors l'application affiche les crédits puis se ferme
                          credits();
                          readln();
                          end;
                    else ecrireEnPosition(getErreurCoord,'Erreur');
                    end;
               end;
end;

procedure menuPrincipal();
var
   Rep:string;
begin
     //Initialisaion reponse
     rep:='0';
     //décor
     cadre(double);
     couleurTexte(10);

     //Texte et coordonnée
     deplacerCurseurXY(1,1);
     writeln('1: ',TexteLac);
     deplacerCurseurXY(1,2);
     writeln('2: ',TexteIUT);
     deplacerCurseurXY(1,3);
     writeln('3: ',TexteForet);
     deplacerCurseurXY(1,4);
     writeln('4: ',TexteDecharge);
     deplacerCurseurXY(1,5);
     writeln('5: ',etatConstruction2);
     deplacerCurseurXY(1,6);
     writeln('6: ',texteCentre);
     deplacerCurseurXY(1,7);
     writeln('7: Personnages');
     deplacerCurseurXY(1,8);
     writeln('8: Dormir');
     deplacerCurseurXY(1,14);
     write('10: Quitter' );
     //Affichage ressource
     synthese();

     // boucle de la réponse
     while (rep<>('1'))and (rep<>('2')) and (rep<>('3')) and (rep<>('4')) and (rep<>('5')) and (rep<>('6')) and (rep<>('7'))and(rep<>('8')) and(rep<>('10')) and(rep<>('76')) {and(rep<>('9'))} do
           begin
                posRep(rep);
                case rep of
                     '1' : DecouvrirLac;                                        //Si rep=1 alors envoit un 1 villageois explorer le Lac
                     '2' : DecouvrirIUT;                                        //Si rep=2 alors envoit un 1 villageois explorer l'IUT
                     '3' : DecouvrirForet;                                      //si rep=3 alors envoit un 1 villageois explorer la foret
                     '4' : DecouvrirDecharge;                                   //si rep=4 alors envoit un villageois explorer la décharge
                     '5' : cliqueFerme;                                         //si rep=4 alors construire ferme ou utiliser
                     '6' : cliqueCentre;                                        //si rep=6 alors constuire centre ou utiliser
                     '7' : detailPerso();                                       //si rep=7 alors donne des détails sur les personnages
                     '8' : finDeTour();                                         //Si rep=8 alors fin du tour
                     '10': read();                                              //si rep=10 alors fin du programme
                     '76': menuCheat();                                         //Si rep=76 alors on ouvre le menu de cheat
                else ecrireEnPosition(getErreurCoord,'Erreur');                 // pour tout autre nombre entré alors afficher "erreur"
                end;
           end;
end;

procedure debutPartie();
begin
     preface();
     effacerEcran();                                                            //Effacer l'écran du menu initial
     initialisationDecouverte;                                                  //Initialise les boolean de decouverte de lieux
     genPerso();                                                                //Generation des 4 personnages
     initTour;                                                                  //Initialisation tour à 1
     initialisationExplo;                                                       //Initialisation exploration
     initialisation();                                                          //Initialisation ressources
     InitialisationCentre;
     gen1Meteo();                                                               //Generation de la météo
     menuPrincipal();                                                           //lancement du menu principal
end;

procedure findetour();
begin

     expFindetour;
     bilanRessource;                                                            //Compte les ressources
     ecranBilan;                                                                //Ecran bilan de la journee

     //Generation et operations
     Decouvertefindetour;
     RenitialisationOccup;                                                      //Reset occupation villageois
     initialisationExplo;                                                       //Reset les endroits visités
     genMeteo;                                                                  //Genère la meteo
     manger();                                                                  //Retire de la nourriture
     tourSuivant;                                                               //Tour+1
     Debloquerexp;


     //Evenements
     aleacombat(15);
     aleamarchand(10);

     menuPrincipal;
end;

procedure synthese();
begin
     //texte récapitulatif état ressources/jour/météo/personnages/constructions
     couleurTexte(6);
     ecrireEnPosition(getWoodCoord,qtwoodSTR);
     couleurTexte(14);
     ecrireEnPosition(getFoodCoord,qtfoodSTR);
     couleurTexte(8);
     ecrireEnPosition(getSteelCoord,qtsteelSTR);
     couleurTexte(7);
     ecrireEnPosition(getMoneyCoord,qtmoneySTR);
     couleurTexte(15);
     deplacerCurseurXY(90,5);
     writeln(qtcomposantSTR);

     setPersonnesInactives(personneLibre);
     couleurTexte(10);
     deplacerCurseurXY(90,7);
     writeln('Personnes inactives : ',getPersonnesInactives);
     deplacerCurseurXY(90,8);
     writeln('Personnes au lac : ',getPersonnesAuLac);
     deplacerCurseurXY(90,9);
     writeln('Personnes à l''IUT : ',getPersonnesAIUT);
     deplacerCurseurXY(90,10);
     writeln('Personnes en forêt : ',getPersonnesEnForet);
     deplacerCurseurXY(90,11);
     writeln('Personnes à la ferme : ',getPersonnesEnExploitationFerme);
     deplacerCurseurXY(90,12);
     writeln('Personnes à la décharge : ',getPersonnesALaDecharge);
     deplacerCurseurXY(90,14);
     writeln('Ferme construite : ',etatConstruction);
     deplacerCurseurXY(90,15);
     writeln('Centre construit : ',etatConstruction2centre);
     deplacerCurseurXY(90,25);
     writeln('Jour : ',getTour);
     deplacerCurseurXY(90,26);
     writeln('Meteo : ',getMeteo);
     couleurTexte(10);

     setPersonnesInactives(personneLibre);
end;

end.

