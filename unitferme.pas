unit unitFerme;

{$mode objfpc}{$H+} {$codepage UTF8}

interface

uses
  Classes, SysUtils, unitRessources, unitCoordonnee, gestionEcran, unitExploration;

procedure constructionFerme();                                                  //procédure de construction de la ferme
procedure cliqueFerme();                                                        //procédure d'exploitation de la fermme pour récolter de la nourriture ou construction de la ferme
function getConstruction():boolean;                                             //fonction pour donner l'état de construction de la ferme
function etatConstruction():string;                                             //fonction pour afficher état de la construction dans le récapitulatif du menu principal à droite
function etatConstruction2():string;                                            //fonction permettant d'afficher deux options dans la liste des actions disponibles du menu principal("construire ferme" puis "cultiver ferme")

implementation
uses
  unitMenu;

var
  fermeConstruite:boolean;  //booléen d'etat de la ferme

function getConstruction (): boolean;
begin
     getConstruction:=fermeConstruite;
end;

function etatConstruction ():string;
begin
     if getConstruction=false then
       etatConstruction:='Non'
     else etatConstruction:='Oui';
end;

procedure constructionFerme ();
begin
  fermeConstruite:=false;

  //Vérification des ressources
  if (getwood>=200) or (getsteel>=100) then
    begin
         fermeConstruite:=true;
         begin
         if getwood>=200 then
            setwood(getwood-200)
         else if getsteel>=100 then
              setsteel(getsteel-100)
         else
           begin
             setsteel(getsteel-getsteel);
             setwood(getwood-getwood);
           end;
         end;
         menuPrincipal
    end

  //Affichage si pas assez de ressources
  else
  begin
  ecrireEnPosition(geterreurcoord,'Vous n''avez pas assez de bois');
  readln();
  menuPrincipal;
  end;



end;

function etatConstruction2 ():string;
begin
  if getConstruction=true then
    etatConstruction2:='Cultiver la ferme'
  else etatConstruction2:='Construire Ferme (200 de bois / ou 100 d''aciers nécessaires)';
end;

procedure cliqueFerme ();
begin
  //Affichage
  if getConstruction=true then
     exploitation ()
  else constructionFerme ();
end;

end.

