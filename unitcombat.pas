unit unitCombat;

{$mode objfpc}{$H+} {$codepage UTF8}

interface

uses
  Classes,SysUtils,gestionEcran,unitCadre,unitRessources;

type
  adversaire=(radcafard,rataupe,radscorpion,fangeux);

procedure attaque;                                                              //Lancer une attaque
function combat():boolean;                                                      //Gère les combats
procedure aleaCombat(pourcentageCombat:integer);                                //Gère les pourcentages de combats
function genAdversaire():string;                                                //Génère le texte suivant l'adversaire
function getAdversaire():adversaire;                                            //Génère l'adversaire

implementation
var
  viePersonnage,                                                                //Vie du Personnage
  viePNJ : Integer;
  monAdversaire:adversaire;                                                     //Vie du PNJ

procedure attaque;
var
  vieTempPerso: integer;                                                        //Vie temporaire du personnage pour la calcul des dégats
  vieTempPNJ: integer;                                                          //Vie temporaire du PNJ pour la calcul des dégats

begin
  //Initialisation
  vieTempPerso := viePersonnage;
  vieTempPNJ := viePNJ;

  //Préparation écran tour 1 + aléatoire
  effacerEcran;
  cadre(double);
  randomize;

  //Tour du Personnage
  writeln('Bilan de l''attaque');
  viePNJ := viePNJ - (random(24)+1);
  deplacerCurseurXY(1,10);
  writeln('Vous avez donné ',vieTempPNJ-viePNJ,' dégat(s)');

  //Tour de PNJ
  viePersonnage := viePersonnage - (random(15)+1);
  deplacerCurseurXY(1,11);
  writeln('Vous avez reçu ',vieTempPerso-viePersonnage,' dégat(s)');
  deplacerCurseurXY(0,0);
  readln;
end;

function combat():boolean;
begin
  //Initialisation
  viePersonnage := 100;
  viePNJ := 100;

  //Affichage
  while (viePNJ > 0) and (viePersonnage > 0) do
        begin
         effacerEcran;
         cadre(double);
         titre('Vous êtes dans un combat',0);
         genAdversaire();
         deplacerCurseurXY(30,12);
         writeln('         />_________________________________');
         deplacerCurseurXY(30,13);
         writeln('[########[]_________________________________>');
         deplacerCurseurXY(30,14);
         writeln('         \>');
         deplacerCurseurXY(1,6);
         writeln('Un ',getAdversaire,' vous attaque!');
         deplacerCurseurXY(1,7);
         writeln('Coup de pied');
         deplacerCurseurXY(1,8);
         writeln('Vie personnage ',viePersonnage,'/100');
         deplacerCurseurXY(1,9);
         writeln('Vie adversaire ',viePNJ,'/100');
         deplacerCurseurXY(0,0);
         readln;
         attaque();

        end;

  effacerEcran;
  cadre(double);

  //Affichage des résultats
  if viePNJ < viePersonnage then
     begin
        titre('Vous avez gagné !',0);
        deplacerCurseurXY(0,0);
        combat := true;
        readln();

     end
  else
     begin
        titre('Vous avez perdu, vous perdez toutes vos ressources',0);
        deplacerCurseurXY(0,0);
        setWood(0);
        setFood(0);
        setSteel(0);
        setMoney(0);
        combat := false;
        readln();
     end;

end;

procedure aleaCombat(pourcentageCombat:integer);
begin
     randomize;
     if random(100)+1<=pourcentageCombat then combat();
end;

function genAdversaire():string;
var
   alea : integer;
begin
  randomize;
  alea:=random(4);
  monAdversaire:=adversaire(alea);
end;

function getAdversaire():adversaire;
begin
  getAdversaire := monAdversaire;
end;
end.

