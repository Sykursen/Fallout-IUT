unit unitCadre;

{$mode objfpc}{$H+} {$codepage UTF8}

interface

uses
  Classes, SysUtils, gestionEcran, unitCoordonnee;

procedure cadre(tCadre : typeBordure);                   //grand cadre autour
procedure titre(texte : String;decalage : integer);      //titre apparaissant au milieu de l'écran
implementation

procedure cadre(tCadre :typeBordure);
begin
     dessinerCadre(getCadre1Coord,getCadre2Coord,tCadre,10,0)
end;

procedure titre(texte : String;decalage : integer);
var
  c : coordonnees;
begin
     C.x:=(118 div 2)-(length(texte)div 2);
     C.y:=5+decalage;
     ecrireEnPosition(c,texte);
end;

end.

