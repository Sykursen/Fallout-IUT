unit unitDate;

{$mode objfpc}{$H+} {$codepage UTF8}

interface

uses
  Classes, SysUtils;

procedure initTour();        //fonction qui initialise les tour à 1
function getTour():integer;  // Getter tour
procedure tourSuivant();     // Setter tour

implementation

var
   nbTour:integer;           //nombre de tour

procedure initTour();
begin
     nbTour:=1;
end;

function getTour():integer; // Getter tour
begin
     getTour := nbTour;
end;

procedure tourSuivant();    // Setter tour
begin
     nbTour := nbTour+1;
end;

end.

