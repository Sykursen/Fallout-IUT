unit unitCheat;

{$mode objfpc}{$H+} {$codepage utf8}

interface

uses
  Classes, SysUtils, unitRessources, GestionEcran, unitCadre, unitReponse, unitCoordonnee;
//Procedure d'ajout de ressources
procedure ajoutWood();
procedure ajoutFood();
procedure ajoutSteel();
procedure ajoutMoney();
//Procedure pour retirer ressources
procedure retireWood();
procedure retireFood();
procedure retireSteel();
procedure retireMoney();
procedure retirerComposant;
procedure ajoutComposant();
procedure menuCheat();
implementation
uses
  unitMenu;

procedure ajoutWood();
begin
  setWood(getWood - 100);
  menuCheat();
end;

procedure ajoutFood();
begin
  setFood(getFood - 100);
  menuCheat();
end;

procedure ajoutSteel();
begin
  setSteel(getSteel - 100);
  menuCheat();
end;

procedure ajoutMoney();
begin
  setMoney(getMoney - 100);
  menuCheat();
end;

procedure retireWood();
begin
  setWood(getWood + 100);
  menuCheat();
end;

procedure retireFood();
begin
  setFood(getFood + 100);
  menuCheat();
end;

procedure retireSteel();
begin
  setSteel(getSteel + 100);
  menuCheat();
end;

procedure retireMoney();
begin
  setMoney(getMoney + 100);
  menuCheat();
end;

procedure retirerComposant;
begin
   setComposant(getComposant - 10);
  menuCheat();
  end;

procedure ajoutComposant();
begin
  setComposant(getComposant + 10);
  menuCheat();
end;

procedure menuCheat();
var repCheat : string;
begin
  //initialisation
  repCheat:='0';
  effacerEcran;
  //texte et coordonné et cadre
  cadre(double);
  deplacerCurseurXY(1,2);
  writeln('1: Retirer du bois (100 pièces)');
  deplacerCurseurXY(1,3);
  writeln('2: Retirer de la nourriture (100 pièces)');
  deplacerCurseurXY(1,4);
  writeln('3: Retirer de l''acier (100 pièces)');
  deplacerCurseurXY(1,5);
  writeln('4: Retirer de l''argent (100 pièces)');
  deplacerCurseurXY(1,6);
  writeln('5: Ajouter du bois (100 pièces)');
  deplacerCurseurXY(1,7);
  writeln('6: Ajouter de la nourriture (100 pièces)');
  deplacerCurseurXY(1,8);
  writeln('7: Ajouter de l''acier (100 pièces)');
  deplacerCurseurXY(1,9);
  writeln('8: Ajouter de l''argent (100 pièces)');
  deplacerCurseurXY(1,10);
  writeln('9: Ajouter des composants (10 pièces)');
  deplacerCurseurXY(1,11);
  writeln('10: Retirer des composant (10 pièces)');
  deplacerCurseurXY(1,12);
  writeln('11: Retour');

  synthese();

  // boucle de la réponse
  while (repCheat<>('1')) AND (repCheat<>('2')) AND (repCheat<>('3')) AND (repCheat<>('4')) AND (repCheat<>('5')) AND (repCheat<>('6')) AND (repCheat<>('7')) AND (repCheat<>('8'))  AND (repcheat<>('9'))AND (repcheat<>('10'))AND (repcheat<>('11')) do
        begin
             posRep(repCheat);
             case repCheat of
                  '1' : ajoutWood;        //Si repcheat=1 alors on ajoute 100 de bois
                  '2' : ajoutFood;        //Si repcheat=2 alors on ajoute 100 de nourriture
                  '3' : ajoutSteel;       //Si repcheat=3 alors on ajoute 100 d'acier
                  '4' : ajoutMoney;       //Si repcheat=4 alors on ajoute 100 d'argent
                  '5' : retireWood;       //Si repcheat=5 alors on retire 100 de bois
                  '6' : retireFood;       //Si repcheat=6 alors on retire 100 de nourriture
                  '7' : retireSteel();    //Si repcheat=7 alors on retire 100 d'acier
                  '8' : retireMoney();    //Si repcheat=8 alors on retire 100 d'argent
                  '9' : ajoutComposant(); //si repcheat=9 alors on ajoute 10 de composant
                  '10': retirerComposant();//si repcheat=10 alors on retire 10 de composant

                  '11' : menuPrincipal; //Si repcheat=11 alors on retourne au menu principal
             else ecrireEnPosition(getErreurCoord,'Erreur');
             end;
        end;
end;

end.

