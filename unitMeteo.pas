unit unitMeteo;

{$mode objfpc}{$H+}  {$codepage UTF8}

interface

uses
  Classes, SysUtils;
type meteo = (Nuageux,Averses,Soleil,Pluies_Acides);

procedure gen1Meteo;                                                            //Procédure qui génére la première météo du jeu
procedure genMeteo;                                                             //Procédure qui génére une nouvelle météo
function getMeteo():meteo;                                                      //procédure qui renvoi la météo actuel

implementation
var
  maMeteo:meteo;          //Météo actuelle

procedure gen1Meteo;
var
  alea : integer;         //nombre aléatoire pour la génération de la météo
begin
  //Génération aléatoire
  randomize;
  alea := random(4);

  //Changement de la météo
  maMeteo := meteo(alea);
end;

procedure genMeteo;
var
  alea : integer;         //nombre aléatoire pour la génération de la météo
  oldMeteo:meteo;         //Ancienne météo
begin
  //Initialisation ancienne météo
  oldMeteo := maMeteo;

  //Génération aléatoire
  randomize;
  alea := random(4);
  //Changement de la météo
  maMeteo := meteo(alea);

  //Test pour ne par avoir 2 jours de suite la même météo
  while maMeteo = oldMeteo do
      begin
      alea := random(4);
      maMeteo := meteo(alea);
      end;
end;

function getMeteo():meteo;
begin
  //Renvoi la météo
  getMeteo := maMeteo;
end;

end.

