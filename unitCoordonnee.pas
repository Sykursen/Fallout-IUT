unit unitCoordonnee;

{$mode objfpc}{$H+} {$codepage UTF8}

interface

uses
  Classes, SysUtils,gestionEcran;

function getFoodCoord() : coordonnees;            //Renvoie les coordonnées pour l'affichage de la nourriture
function getWoodCoord() : coordonnees;            //Renvoie les coordonnées pour l'affichage du bois
function getSteelCoord() : coordonnees;           //Renvoie les coordonnées pour l'affichage de l'acier
function getMoneyCoord() : coordonnees;           //Renvoie les coordonnées pour l'affichage de l'argent
function getIniCoord(): coordonnees;              //Renvoie les coordonnées pour la position initiale du curseur
function getCadreRepCoord1(): coordonnees;        //Renvoie les coordonnées du cadre de réponse
function getCadreRepCoord2(): coordonnees;        //Renvoie les coordonnées du cadre de réponse
function getErreurCoord():coordonnees;            //Renvoie les coordonnées du message d'erreur
function getCadre1Coord():coordonnees;            //Renvoie les coordonnées du cadre principal
function getCadre2Coord():coordonnees;            //Renvoie les coordonnées du cadre principal



implementation
var
foodCoord:coordonnees;                          //Variable de coordonnées pour l'affichage de la nourriture
woodCoord:coordonnees;                          //Variable de coordonnées pour l'affichage du bois
steelCoord:coordonnees;                         //Variable de coordonnées pour l'affichage de l'acier
moneyCoord:coordonnees;                         //Variable de coordonnées pour l'affichage de l'argent
iniCoord :coordonnees;                          //Variable de coordonnées pour la position initiale du curseur
cadreRepCoord1:coordonnees;                     //Variable de coordonnées cadre de réponse
cadreRepCoord2:coordonnees;                     //Variable de coordonnées cadre de réponse
erreurCoord:coordonnees;                        //Variable de coordonnées du message d'erreur
cadre1Coord:coordonnees;                        //Variable de coordonnées du cadre principal haut gauche
cadre2Coord:coordonnees;                        //Variable de coordonnées du cadre principal bas

function getFoodCoord() : coordonnees;
begin
     foodCoord.x :=90;
     foodCoord.y :=1;
     getFoodCoord:=foodCoord;
end;

function getWoodCoord() : coordonnees;
begin
     woodCoord.x :=90;
     woodCoord.y :=2;
     getWoodCoord:=woodCoord;
end;

function getSteelCoord() : coordonnees;
begin
     steelCoord.x :=90;
     steelCoord.y :=3;
     getSteelCoord:=steelCoord;
end;

function getMoneyCoord() : coordonnees;
begin
     moneyCoord.x :=90;
     moneyCoord.y :=4;
     getMoneyCoord:=moneyCoord;
end;

function getIniCoord() : coordonnees;
begin
     iniCoord.x :=1;
     iniCoord.y :=28;
     getIniCoord:=inicoord;
end;

function getCadreRepCoord1(): coordonnees;
begin
     cadreRepCoord1.x:=0;
     cadreRepCoord1.y:=27;
     getCadreRepCoord1:=cadreRepCoord1;
end;

function getCadreRepCoord2(): coordonnees;
begin
     cadreRepCoord2.x:=119;
     cadreRepCoord2.y:=29;
     getCadreRepCoord2:=cadreRepCoord2;
end;

function getErreurCoord(): coordonnees;
begin
     erreurCoord.x:=1 ;
     erreurCoord.y:=26  ;
     getErreurCoord:=erreurCoord ;
end;

function getCadre1Coord():coordonnees;
begin
     cadre1Coord.x:= 0;
     cadre1Coord.y:= 0;
     getCadre1Coord:=cadre1Coord;
end;

function getCadre2Coord():coordonnees;
begin
     cadre2Coord.x:= 119;
     cadre2Coord.y:= 29;
     getCadre2Coord:=cadre2Coord;
end;

end.

