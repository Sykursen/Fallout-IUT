unit UnitDecouverte;

{$mode objfpc}{$H+}  {$codepage UTF8}

interface

uses
  Classes, SysUtils,unitExploration,unitGestionPersonnage;

procedure initialisationDecouverte();                                           //Initialise les états de découverte à faux au premier tour
procedure DecouvrirLac;                                                         //procédures permetttant de gérer si oui ou non le joueur a déjà exploré les lieux
procedure DecouvrirForet;
procedure DecouvrirIUT;
procedure DecouvrirDecharge;
function TexteLac():string;
function TexteForet():string;                                                   //Fonctions permettant de modifier le texte du menu, suivant l'exploration ou non
function TexteDecharge():string;
function TexteIUT():string;
procedure Decouvertefindetour();                                                //Change le texte si les conditions son vraies à la fin du tour

implementation
uses
  unitMenu;

  var
  //booléens de decouverte des lieux
  EtatDecouverteLac:boolean;
  EtatDecouverteForet:boolean;
  EtatDecouverteDecharge:boolean;
  EtatDecouverteIUT:boolean;

  //Booléens qui permettent d'activer ou non la decouverte de lieux au prochain tour
  EtatDecouverteForetFIN:boolean;
  EtatDecouverteDechargeFIN:boolean;
  EtatDecouverteIUTFIN:boolean;
  EtatDecouverteLacFIN:boolean;


procedure initialisationDecouverte();
 begin
//Booléens d'état de la découverte
   EtatDecouverteLac:=false;
   EtatDecouverteForet:=false;
   EtatDecouverteDecharge:=false;
   EtatDecouverteIUT:=false;
//Booléens qui permettent de dire si en fin de tour le boolean d'etat associé doit passer à vrai ou non
   EtatDecouverteLacFIN:=false;
   EtatDecouverteForetFIN:=false;
   EtatDecouverteDechargeFIN:=false;
   EtatDecouverteIUTFIN:=false;
 end;

procedure DecouvrirLac;
begin
  //Si decouvert alors procedure normale
  if  EtatDecouverteLac=True then ExplorationLac
 //sinon Setup l'exploration
 else
   begin
        setLieuchoisipourattribution(Exploration);
        if getPersonnesInactives>0 then  setPersonnesInactives(getPersonnesInactives-1)
        else setPersonnesInactives(0);
        attributionPersonnage;
        EtatDecouverteLacFIN:=True;
        menuPrincipal;
  end;
end;

procedure DecouvrirIUT;
begin
     //Si decouvert alors procedure normal
     if  EtatDecouverteIUT=True then ExplorationIUT
     //sinon Setup l'exploration
     else
       begin
            setLieuchoisipourattribution(Exploration);
            if getPersonnesInactives>0 then  setPersonnesInactives(getPersonnesInactives-1);
            attributionPersonnage;
            EtatDecouverteIUTFIN:=True;
            menuPrincipal;
       end;
end;

procedure DecouvrirForet;
begin
     //Si decouvert alors procédure normale
     if  EtatDecouverteForet=True then ExplorationForet
     else
       begin
       //sinon Setup l'exploration
       setLieuchoisipourattribution(Exploration);
       if getPersonnesInactives>0 then  setPersonnesInactives(getPersonnesInactives-1);
       attributionPersonnage;
       EtatDecouverteForetFIN:=True;
       menuPrincipal;
     end;
end;

procedure DecouvrirDecharge;
begin
     //Si découvert alors procédure normale
     if  EtatDecouverteDecharge=True then ExplorationDecharge
     else
     begin
          //sinon Setup l'exploration
          setLieuchoisipourattribution(Exploration);
          if getPersonnesInactives>0 then  setPersonnesInactives(getPersonnesInactives-1);
          attributionPersonnage;
          EtatDecouverteDechargeFIN:=True;
          menuPrincipal;
     end;
end;

procedure Decouvertefindetour();
begin
     if EtatDecouverteLacFIN=True then EtatDecouverteLac:=True;
     if EtatDecouverteForetFIN=True then EtatDecouverteForet:=True;
     if EtatDecouverteDechargeFIN=True then EtatDecouverteDecharge:=True;
     if EtatDecouverteIUTFIN=True then EtatDecouverteIUT:=True
end;

function TexteLac():string;
begin
     if EtatDecouverteLac=false  then
     TexteLac:='Explorer le lac'
     else TexteLac:='Envoyer quelqu''un au lac'
end;

function TexteForet():string;
begin
     if EtatDecouverteForet=false  then
     TexteForet:='Explorer la Foret'
     else TexteForet:='Envoyer quelqu''un a la forêt'
end;

function TexteIUT():string;
begin
     if EtatDecouverteIUT=false  then
     TexteIUT:='Explorer l''IUT'
     else TexteIUT:='Envoyer quelqu''un a l''IUT '
end;

function TexteDecharge():string;
begin
     if EtatDecouverteDecharge=false  then
     TexteDecharge:='Explorer la decharge'
     else TexteDecharge:='Envoyer quelqu''un a la decharge'
end;

end.

